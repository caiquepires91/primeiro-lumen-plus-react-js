<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//use Illuminate\Contracts\Routing\ResponseFactor;
use App\Models\Todo;

class TodosController extends Controller
{
    public function all() {
        return Todo::get();
    }

    public function destroy(Request $request) {
        $id = $request->input('id');
        Todo::whereId($id)->delete(); //check if this notation works;
        return response()->json(['success'=>true]);
    }

    public function store(Request $request) {
        $todo = new Todo;
        $todo->body = $request->input('body');
        $todo->done = 0;
        if ($todo->save()) return response()->json(['success'=>true, 'todo'=>$todo]);
        else return response()->json(['success'=>false]);
    }

    public function done(Request $request) {
        $id = $request->input('id');
        $todo = Todo::where('id', $id)->first();
        if ($todo) {
            $todo->done = true;
            if ($todo->save()) $response = ['success'=>true, 'todo'=>$todo];
        } else {
            $response = ['success'=>false];
        }
       // $response = ['id'=>$id];
        return response()->json($response);
    }

    public function unDone(Request $request) {
        $id = $request->input('id');
        $todo = Todo::find($id);
        if ($todo) {
            $todo->done = false;
            if ($todo->save()) $response = ['success'=>true, 'todo'=>$todo];
        } else {
            $response = ['success'=>false];
        }
        return response()->json($response);
    }
 }
