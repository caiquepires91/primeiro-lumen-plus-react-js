<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Todo;

class TodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Todo::create([
            'body' => 'Lavar roupas brancas',
            'done' => 0
        ]);

        Todo::create([
            'body' => 'Comprar frutas',
            'done' => 0
        ]);

        Todo::create([
            'body' => 'Fazer faxina',
            'done' => 1
        ]);

        Todo::create([
            'body' => 'Arrumar o quarto',
            'done' => 1
        ]);
    }
}
