/* ReactDOM.render(
    React.createElement('div', null,
        React.createElement('h1', null, 'Qual seu nome?'),
        React.createElement('form', {method: 'POST'},
            React.createElement('input', {placeholder: 'Digite seu nome'}),
            React.createElement('button', {type: 'submit'}, 'Enviar')
            ),
    ),
    document.querySelector('#app-1')
); */

ReactDOM.render(<div>Patinhos verdes</div>, document.querySelector('#app'));

ReactDOM.render(
    <div>
        <h1>Qual seu nome?</h1>
        <form method="POST">
            <input placeholder="Digite seu nome"/>
            <button type='submit'>Enviar</button>
        </form>
    </div>
    ,document.querySelector('#app-2')
);


// Atualmente não há diferenças entre Funções e Classes, fica a critério do programador.
// Mas segundo programadores:
// React Hooks > React Lifecycle
// por isso: componente funcional > componente de classe

function Colsm (props) {
    const codeInserido = props.children;
    return (<div class="col-sm">{codeInserido}</div>)
};

ReactDOM.render(
    <div class='row'>
        <Colsm>será que pode ir outro html <a href="#">link</a></Colsm>
        <Colsm>será que pode coluna 2 html <a href="#">link</a></Colsm>
        <Colsm>será que pode ir coluna 3 html <a href="#">link</a></Colsm>
    </div>,
    document.querySelector('#app-1')
)

function Formulario1 ({ texto }) {
    return (
        <div>
            <h1> { texto } </h1>
            <form method="POST">
                <input placeholder="Digite seu nome"/>
                <button type='submit'>Enviar</button>
            </form>
        </div>
    )
};

ReactDOM.render(
    <Formulario1 texto="Qual seu nome?"/>,
    document.querySelector('#app-3')
);

class Formulario2 extends React.Component {
    constructor(props, context){
        super(props, context);
        this.render = this.render.bind(this);
    }

    render () {
        return (
            <div>
                <h1>{ this.props.texto }</h1>
                <form method="POST">
                    <input placeholder="Digite seu nome"/>
                    <button type='submit'>Enviar</button>
                </form>
            </div>
        )
    }
}

ReactDOM.render(
    <Formulario2 texto="Qual seu nome?" />,
    document.querySelector('#app-4')
);

function CapsLock(props) {
    const textoInserido = props.children;
    const textoEmCapsLock = textoInserido.toUpperCase();
    return (<div>{textoEmCapsLock}</div>)
}

ReactDOM.render(
    <CapsLock>texto era minusculo aqui aqui</CapsLock>,
    document.querySelector('#app-5')
);

function Contador() {
    const [contador,setContador] = React.useState(1);

    function adicionarContador() {
        setContador(contador + 1);
        console.log("Adicionou");
    }

    return (
        <div>
            <div>{contador}</div>
            <button onClick={adicionarContador}>Adicionar</button>
        </div>
    )
}

ReactDOM.render(
    <Contador/>,
    document.querySelector('#app-5')
);