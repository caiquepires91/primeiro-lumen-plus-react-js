<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset="UTF-8">
    <title>Lumen + React - To Do App</title>
    <!-- bootstrap css -->
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
	<link href="css/index.css" rel="stylesheet">
    <style>
    </style>
</head>
<body>
    <div id="app"></div>
    <script> var API_URL = "{{url('/')}}"; </script>
    <script src="js/build.js" type="text/babel"></script>
</body>
</html>