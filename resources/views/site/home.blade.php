@extends('site.master')
@section('content')
	<div id="app"></div>
	<div id="app-1"></div>
	<div id="app-2"></div>
	<div id="app-3"></div>
	<div id="app-4"></div>
	<div id="app-5"></div>
	<div id="app-6"></div>

	<!-- * react code goes inside script text/babel, because we're using babel standalone type -->
	<script> var API_URL = "{{url('/')}}"; </script>
	<script src="js/build.js"></script>
@stop