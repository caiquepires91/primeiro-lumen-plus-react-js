<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', 'Home@index');
$router->get('todos', 'TodosController@all');
$router->post('todos/destroy', 'TodosController@destroy');
$router->post('todos/store', 'TodosController@store');
$router->post('todos/done', 'TodosController@done');
$router->post('todos/undone', 'TodosController@unDone');