import React, { Component } from 'react'

// components previously created
import TodoList from './TodoList';
import TodoForm from './TodoForm';

export default class TodoApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todos : []
        }
    }

    fetchTodoList() {
        fetch(API_URL + '/todos').then((response)=> {
            response.json().then((jsonResponse)=> {
                this.setState( { todos: jsonResponse } );
            });
        });
    }

    render () {
        return (
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <h1>Falta(m) {this.remaining()} de {this.state.todos.length} </h1>
                    <TodoForm onTodoStore={this.handleNewTodo.bind(this)}/>
                    <div className="ui divider"></div>
                    <TodoList
                        todos = {this.state.todos}
                        onDeleteTodo = {this.onDeleteTodo.bind(this)}
                        onMarkAs = {this.onMarkAs.bind(this)}
                    />
                    <div className="ui divider"></div>
                </div>
            </div>
        )
    }

    onDeleteTodo(todo) {
        if(!confirm('Tem certeza?')) return;

        let index = this.state.todos.indexOf(todo),
            self = this,
            data = new FormData();

        data.append('id', todo.id);
        fetch(API_URL + '/todos/destroy', {method: 'post', body: data}).then(
            (response)=> {
                response.json().then(
                    (jsonResponse) => {
                        if (jsonResponse.success) {
                            self.setState({ todos: self.state.todos.filter((_, i) => i !== index) });
                        }
                    }
                );
            }
        );
    }

    onMarkAs(todo, state = 'done') {
        console.log("aqui");
        console.log("data id: " + todo.id);
        
        let url = state == 'done' ? API_URL + "/todos/done" : API_URL + "/todos/undone",
            data = new FormData(),
            index = this.state.todos.indexOf(todo),
            self = this;

            data.append('id', todo.id);

        fetch(url, {method: 'post', body: data}).then((response)=> {
            response.json().then((jsonResponse)=>{
                if (jsonResponse.success) {
                    console.log("resposta mark as: " + jsonResponse);
                    let todos = self.state.todos;
                    todos[index].done = jsonResponse.todo.done;
                    self.setState({todos: todos});
                } else {
                    console.log("resposta mark as: NOT success");
                }
            });
        });
    }

    remaining() {
        //console.log("remaining: " + this.state.todos);
        const undoneTodos = this.state.todos.filter(todo => todo.done == '0');
        return undoneTodos.length;
    }

    handleNewTodo(todo) {
        this.setState({ todos: this.state.todos.concat(todo)});
    }
}