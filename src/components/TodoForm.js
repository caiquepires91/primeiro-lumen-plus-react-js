import React, { Component } from 'react'

export default class TodoForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            body:'',
            working: false
        }
    }

    render() {
        let loading = this.state.working ? 'loading' : '';
        return (
            <div>
                <div className="ui action input fluid">
                    <input type="text" placeholder="Add novo..." value={this.state.body} onChange={this.handleInputChange.bind(this)}/>
                    <button className={'ui button blue ' +  loading} onClick={this.handleClick.bind(this)}>
                        <i className="plus icon"></i>Adicionar
                    </button>
                </div>
            </div>
        );
    }

    handleInputChange(event) {
        this.setState({ body: event.target.value });
    }

    handleClick(event) {
        if(this.state.body.trim() == '') return;
        this.setState({ working: true })

        let self = this, data = new FormData();

        data.append('body', self.state.body); // append the text to FormData

        fetch(API_URL + "/todos/store", {
            method: 'post',
            body: data
        }).then((response) => {
            response.json().then((jsonResponse) => {
                if (jsonResponse.success) {
                    self.props.onTodoStore(jsonResponse.todo);
                    //console.log("todoform: " + jsonResponse.todo.done);
                    self.setState({ body: ''}); // empty text input
                    this.setState({ working: false }); 
                }
            });
        });
    }
}