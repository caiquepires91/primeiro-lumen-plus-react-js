import React from 'react'

const TodoListItem = ({ todo, deleteTodo, markAs }) => {
    console.log("todolistitem todo id: " + todo.id);
    let isDone = <a href="#" title="Mark as done" onClick={ ()=> markAs(todo) }><i class="far fa-square"></i></a>,
        doneClass = '';

    if (todo.done) {
        doneClass = 'todo-done';
        isDone = <a href="#" title="Mark as undone" onClick={ ()=> markAs(todo, 'undone') }><i class="fas fa-check"></i></a>
    }

    return (
        <div className={'ui divided list ' + doneClass}>
            <div className="item row justify-content-md-center">
                <div className="content col-md-9 col-sm-10">
                    <p>{todo.body}</p>
                </div>
                <div className="right floated content col-md-auto">
                    <div className="row justify-content-md-center">
                        <div className="col">{isDone}</div>
                        <div className="col">
                            <a href="#" title="Remove" onClick={ ()=> deleteTodo(todo) }><i class="fas fa-trash-alt"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TodoListItem;