module.exports = {
    mode: 'development',
    entry: './src/index.js',
    output: {
        path: __dirname + '/public/js',
        filename: 'build.js'
    },
    module: {
       
        rules: [
          {  test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            use: {
              loader: "babel-loader"
            } 
          }
        ]
      }
}